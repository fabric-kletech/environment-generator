from shim import Shim
from conf import Configuration
orgCnt = Configuration.returnOrgCnt()
fp = open("env.sh","w")
fp.write("export PATH=${PWD}/../fabric-samples/bin:${PWD}:$PATH\n")
fp.write("export ORGS=\"" + str(Configuration.returnOrgCnt())+"\"\n")
fp.write("export NODES=\"" + str(Configuration.returnOrgCnt())+"\"\n")
fp.write("export ORDERER0_HOSTNAME=\"" + str(Configuration.returnNodeHostname(0))+"\"\n")
fp.write("export FABRIC_CFG_PATH=\"${PWD}\"\n")
for i in range(orgCnt):
    fp.write("export ORG" + str(i+1)+"_"+"PEERS=\""+ str(Configuration.returnPeerCnt(i))+"\"\n")
    fp.write("export NODE" + str(i+1)+"_"+"IP=\""+str(Configuration.returnNodeIP(i))+"\"\n")
    fp.write("export FABRIC_CFG_PATH" + str(i+1)+ "=\""+str(Configuration.returnNodePath(i))+"\"\n")
    fp.write("export ORG" + str(i+1)+"_"+"HOSTNAME=\""+str(Configuration.returnNodeHostname(i))+"\"\n")
fp.write("export SWARM_NETWORK=\"" + str(Configuration.returnStackName())+"\"\n")
fp.write("export DOCKER_STACK=\"" + str(Configuration.returnStackName())+"\"\n")
fp.write("export VERBOSE=false\n")
fp.write("export SYS_CHANNEL=\"bymn-sys-channel\"\n")
