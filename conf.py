import json
class Configuration(object):
    fp = open('conf.json',"r")
    data = json.load(fp)
    @staticmethod
    def printData():
        print(Configuration.data)
    @staticmethod
    def returnOrgCnt():
        return len(Configuration.data["Orgs"])

    @staticmethod
    def returnPeerCnt(x):
        return Configuration.data["Orgs"][x]["peerCnt"]

    @staticmethod
    def returnNodeIP(x):
        return Configuration.data['Nodes'][x]["ip"]
    
    @staticmethod
    def returnNodeHostname(x):
        return Configuration.data['Nodes'][x]["hostname"]
    
    @staticmethod
    def returnNodePath(x):
        return Configuration.data['Nodes'][x]["path"]

    @staticmethod
    def returnChannelName():
        return Configuration.data['Channel_name']

    @staticmethod
    def returnNetworkName():
        return Configuration.data['Net_name']   

    @staticmethod
    def returnStackName():
        return Configuration.data['Stack']  
    @staticmethod
    def returnOrgName(x):
        return Configuration.data['Orgs'][x]['name']

if __name__ == "__main__":
    Configuration.printData()
    print(Configuration.returnOrgCnt())
    print(Configuration.returnPeerCnt(1))
    print(Configuration.returnNodeIP(1))
    print(Configuration.returnNodeHostname(1))
    print(Configuration.returnChannelName())
    print(Configuration.returnNetworkName())
